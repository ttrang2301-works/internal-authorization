package ttrang2301.sample.urp.gateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UrpGatewayApplication {

	public static void main(String[] args) {
		SpringApplication.run(UrpGatewayApplication.class, args);
	}

}
