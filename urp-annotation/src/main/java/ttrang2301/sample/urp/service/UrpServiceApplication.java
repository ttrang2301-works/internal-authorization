package ttrang2301.sample.urp.service;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UrpServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(UrpServiceApplication.class, args);
	}

}
